package com.example.archeryapp.util

import com.example.archeryapp.data.ArchEvent
import com.example.archeryapp.data.ArchHistory
import com.example.archeryapp.data.ArchNews
import com.example.archeryapp.data.ArchSplashResponse
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.GET
import retrofit2.http.POST

interface ArchServerClient {

    @FormUrlEncoded
    @POST("ArcheryApp/splash.php")
    fun getSplash(@Field("locale") locale: String
                  , @Field("simLanguage") simLanguage: String
                  , @Field("model") model: String
                  , @Field("timezone") timezone: String): Call<ArchSplashResponse>

    @GET("ArcheryApp/history.json")
    fun getArchHistory(): Call<ArchHistory>

    @GET("ArcheryApp/tournaments.json")
    fun getArchEvents(): Call<List<ArchEvent>>

    @GET("ArcheryApp/news.json")
    fun getArchNews(): Call<List<ArchNews>>

    companion object {
        fun create() : ArchServerClient {
            val retrofit = Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl("http://195.201.125.8/")
                .build()
            return retrofit.create(ArchServerClient::class.java)
        }
    }

}