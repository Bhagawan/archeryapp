package com.example.archeryapp

import android.graphics.Bitmap
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.appcompat.content.res.AppCompatResources
import androidx.constraintlayout.widget.ConstraintLayout
import com.example.archeryapp.databinding.ActivityMainBinding
import com.example.archeryapp.view.adapters.ArchViewPagerAdapter
import com.google.android.material.tabs.TabLayoutMediator
import com.squareup.picasso.Picasso
import com.squareup.picasso.Target
import java.lang.Exception

class MainActivity : AppCompatActivity() {
    private lateinit var target: Target
    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)

        binding.archViewPager.adapter = ArchViewPagerAdapter(this)
        TabLayoutMediator(binding.archLayoutTabs, binding.archViewPager) { tab, position ->
            when(position) {
                0 -> tab.icon = AppCompatResources.getDrawable(this, R.drawable.ic_baseline_newspaper_24)
                1 -> tab.icon = AppCompatResources.getDrawable(this, R.drawable.ic_trophy)
                2 -> tab.icon = AppCompatResources.getDrawable(this, R.drawable.ic_info)
            }
            tab.text = ""
        }.attach()
        loadArchBackground()
        setContentView(binding.root)
    }

    private fun loadArchBackground() {
        target = object: Target {
            override fun onBitmapLoaded(bitmap: Bitmap?, from: Picasso.LoadedFrom?) {
                bitmap?.let {
                    binding.layoutArchMain.background = BitmapDrawable(resources, it)
                }
            }

            override fun onBitmapFailed(e: Exception?, errorDrawable: Drawable?) {}
            override fun onPrepareLoad(placeHolderDrawable: Drawable?) {}
        }
        Picasso.get().load("http://195.201.125.8/ArcheryApp/back.png").into(target)
    }
}