package com.example.archeryapp.data

import androidx.annotation.Keep
import com.google.gson.annotations.SerializedName

@Keep
data class ArchHistory(@SerializedName("img") val img: String?, @SerializedName("text") val text: String)
