package com.example.archeryapp.data

import androidx.annotation.Keep

@Keep
class ArchSplashResponse(val url : String)