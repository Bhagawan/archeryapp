package com.example.archeryapp.data

import androidx.annotation.Keep
import com.google.gson.annotations.SerializedName

@Keep
data class ArchNewsPiece(@SerializedName("type") val type: String, @SerializedName("body") val body: String)
