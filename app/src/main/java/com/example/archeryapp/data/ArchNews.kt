package com.example.archeryapp.data

import androidx.annotation.Keep
import com.google.gson.annotations.SerializedName

@Keep
data class ArchNews(@SerializedName("name") val name: String, @SerializedName("preview") val previewUrl: String, @SerializedName("text") val news: List<ArchNewsPiece>)
