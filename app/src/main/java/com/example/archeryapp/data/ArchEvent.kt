package com.example.archeryapp.data

import androidx.annotation.Keep
import com.google.gson.annotations.SerializedName

@Keep
data class ArchEvent(@SerializedName("name") val name: String,
                     @SerializedName("logo") val logo: String,
                     @SerializedName("location") val location: String,
                     @SerializedName("locationFlag") val locationFlag: String,
                     @SerializedName("date") val date: String)
