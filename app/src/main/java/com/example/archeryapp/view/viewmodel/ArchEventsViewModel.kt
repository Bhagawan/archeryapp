package com.example.archeryapp.view.viewmodel

import androidx.databinding.BaseObservable
import androidx.databinding.Bindable
import com.example.archeryapp.BR
import com.example.archeryapp.data.ArchEvent
import com.example.archeryapp.util.ArchServerClient
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ArchEventsViewModel: BaseObservable() {

    @Bindable
    var events = emptyList<ArchEvent>()

    init {
        ArchServerClient.create().getArchEvents().enqueue(object : Callback<List<ArchEvent>> {
            override fun onResponse(
                call: Call<List<ArchEvent>>,
                response: Response<List<ArchEvent>>
            ) {
                response.body()?.let {
                    events = it
                    notifyPropertyChanged(BR.events)
                }
            }
            override fun onFailure(call: Call<List<ArchEvent>>, t: Throwable) {}
        })
    }

}