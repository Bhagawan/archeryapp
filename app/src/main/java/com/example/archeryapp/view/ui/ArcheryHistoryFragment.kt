package com.example.archeryapp.view.ui

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.archeryapp.databinding.FragmentArcheryHistoryBinding
import com.example.archeryapp.view.viewmodel.ArchHistoryViewModel

class ArcheryHistoryFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val binding = FragmentArcheryHistoryBinding.inflate(layoutInflater)
        binding.viewModel = ArchHistoryViewModel()
        return binding.root
    }

}