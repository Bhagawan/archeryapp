package com.example.archeryapp.view.adapters

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.databinding.library.baseAdapters.BR
import androidx.recyclerview.widget.RecyclerView
import com.example.archeryapp.R
import com.example.archeryapp.data.ArchNews
import com.example.archeryapp.view.viewmodel.ArchNewsItemViewModel

class ArchNewsAdapter(val listener: ArchNewsInterface) : RecyclerView.Adapter<ArchNewsAdapter.ViewHolder>(), ArchBindableAdapter<List<ArchNews>> {
    private var news = emptyList<ArchNews>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding: ViewDataBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.context), R.layout.item_news, parent, false)
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(news[position])
    }

    override fun getItemCount(): Int {
        return news.size
    }

    @SuppressLint("NotifyDataSetChanged")
    override fun setData(data: List<ArchNews>) {
        news = data
        notifyDataSetChanged()
    }

    inner class ViewHolder(private val binding: ViewDataBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(news: ArchNews) {
            binding.setVariable(BR.viewModel, ArchNewsItemViewModel(news))
            binding.root.setOnClickListener { listener.onClick(news)}
        }
    }

    interface ArchNewsInterface {
        fun onClick(news: ArchNews)
    }
}