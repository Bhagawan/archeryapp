package com.example.archeryapp.view.viewmodel

import android.view.View
import com.example.archeryapp.data.ArchNews

class ArchNewsPopupViewModel(val news: ArchNews, val listener: ArchOnClose) {

    fun close(v: View) = listener.onClose()

    interface ArchOnClose {
        fun onClose()
    }
}