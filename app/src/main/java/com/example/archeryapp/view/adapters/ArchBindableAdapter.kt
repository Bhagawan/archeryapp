package com.example.archeryapp.view.adapters

interface ArchBindableAdapter<T> {
    fun setData(data: T)
}