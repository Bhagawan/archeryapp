package com.example.archeryapp.view.ui

import android.annotation.SuppressLint
import android.content.Context
import android.os.Bundle
import android.view.*
import androidx.fragment.app.Fragment
import android.widget.LinearLayout
import android.widget.PopupWindow
import androidx.databinding.DataBindingUtil
import androidx.databinding.library.baseAdapters.BR
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.archeryapp.R
import com.example.archeryapp.data.ArchNews
import com.example.archeryapp.databinding.FragmentArcheryNewsBinding
import com.example.archeryapp.databinding.PopupNewsItemBinding
import com.example.archeryapp.view.adapters.ArchNewsAdapter
import com.example.archeryapp.view.adapters.ArchNewsPieceAdapter
import com.example.archeryapp.view.viewmodel.ArchNewsPopupViewModel
import com.example.archeryapp.view.viewmodel.ArchNewsViewModel

class ArcheryNewsFragment : Fragment() {
    lateinit var binding: FragmentArcheryNewsBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentArcheryNewsBinding.inflate(layoutInflater)
        binding.viewModel = ArchNewsViewModel()
        binding.recyclerArchNews.layoutManager = LinearLayoutManager(context)
        binding.recyclerArchNews.adapter = ArchNewsAdapter(object: ArchNewsAdapter.ArchNewsInterface {
            override fun onClick(news: ArchNews) = showNewsPopup(news)
        })

        return binding.root
    }

    @SuppressLint("InflateParams")
    private fun showNewsPopup(news: ArchNews) {
        val popupBinding: PopupNewsItemBinding = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.popup_news_item, null, false)

        val width = LinearLayout.LayoutParams.MATCH_PARENT
        val height = LinearLayout.LayoutParams.MATCH_PARENT

        val popupWindow = PopupWindow(popupBinding.root, width, height, false)
        val popupViewModel = ArchNewsPopupViewModel(news, object : ArchNewsPopupViewModel.ArchOnClose {
            override fun onClose() {
                popupWindow.dismiss()
            }
        })
        popupBinding.recyclerArchPopupNewsPiece.layoutManager = LinearLayoutManager(context)
        popupBinding.recyclerArchPopupNewsPiece.adapter = ArchNewsPieceAdapter()

        popupBinding.setVariable(BR.viewModel, popupViewModel)

        popupWindow.animationStyle = R.style.PopupArchAnimation
        popupWindow.showAtLocation(binding.root, Gravity.CENTER, 0, 0)

        val p = popupWindow.contentView.rootView.layoutParams as WindowManager.LayoutParams
        p.flags = p.flags or WindowManager.LayoutParams.FLAG_DIM_BEHIND or WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS
        p.dimAmount = 0.26f

        context?.let {
            val wm: WindowManager = it.getSystemService(Context.WINDOW_SERVICE) as WindowManager
            wm.updateViewLayout(popupWindow.contentView.rootView, p)
        }
    }

}