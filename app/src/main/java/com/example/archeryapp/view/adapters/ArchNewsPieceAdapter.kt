package com.example.archeryapp.view.adapters

import android.annotation.SuppressLint
import android.graphics.Color
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.example.archeryapp.data.ArchNews
import com.example.archeryapp.data.ArchNewsPiece
import com.squareup.picasso.Picasso

class ArchNewsPieceAdapter: RecyclerView.Adapter<ArchNewsPieceAdapter.ViewHolder>(), ArchBindableAdapter<ArchNews> {
    private var info = emptyList<ArchNewsPiece>()

    companion object {
        const val IMAGE = 1
        const val TEXT = 2
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return when(viewType) {
            IMAGE -> {
                val view = ImageView(parent.context)
                view.layoutParams = ConstraintLayout.LayoutParams(ConstraintLayout.LayoutParams.MATCH_PARENT, 200)
                ViewHolder(view)
            }
            else -> {
                val view = TextView(parent.context)
                view.textSize = 14.0f
                view.setTextColor(Color.BLACK)
                view.textAlignment = View.TEXT_ALIGNMENT_TEXT_START
                view.layoutParams = ConstraintLayout.LayoutParams(ConstraintLayout.LayoutParams.MATCH_PARENT, ConstraintLayout.LayoutParams.WRAP_CONTENT)
                ViewHolder(view)
            }
        }
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        when(holder.itemViewType) {
            IMAGE -> Picasso.get().load(info[position].body).into(holder.view as ImageView)
            else -> (holder.view as TextView).text = info[position].body
        }
    }

    override fun getItemCount(): Int {
        if(info == null) return 0
        return info.size
    }

    @SuppressLint("NotifyDataSetChanged")
    override fun setData(data: ArchNews) {
        info = data.news
        notifyDataSetChanged()
    }

    override fun getItemViewType(position: Int): Int {
        return when(info[position].type) {
            "image" -> IMAGE
            else -> TEXT
        }
    }

    inner class ViewHolder(val view: View) : RecyclerView.ViewHolder(view)
}