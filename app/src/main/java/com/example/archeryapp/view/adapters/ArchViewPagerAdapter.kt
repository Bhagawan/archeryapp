package com.example.archeryapp.view.adapters

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.example.archeryapp.view.ui.ArcheryEventsFragment
import com.example.archeryapp.view.ui.ArcheryHistoryFragment
import com.example.archeryapp.view.ui.ArcheryNewsFragment

class ArchViewPagerAdapter (fragmentActivity: FragmentActivity) : FragmentStateAdapter(fragmentActivity) {

    override fun getItemCount(): Int = 3

    override fun createFragment(position: Int): Fragment = when(position) {
        0 -> ArcheryNewsFragment()
        1 -> ArcheryEventsFragment()
        else -> ArcheryHistoryFragment()
    }
}