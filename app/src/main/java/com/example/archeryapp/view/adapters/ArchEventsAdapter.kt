package com.example.archeryapp.view.adapters

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.databinding.library.baseAdapters.BR
import androidx.recyclerview.widget.RecyclerView
import com.example.archeryapp.R
import com.example.archeryapp.data.ArchEvent
import com.example.archeryapp.view.viewmodel.ArchEventItemViewModel

class ArchEventsAdapter : RecyclerView.Adapter<ArchEventsAdapter.ViewHolder>(), ArchBindableAdapter<List<ArchEvent>> {
    private var events = emptyList<ArchEvent>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding: ViewDataBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.context), R.layout.item_event, parent, false)
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(events[position])
    }

    override fun getItemCount(): Int {
        return events.size
    }

    @SuppressLint("NotifyDataSetChanged")
    override fun setData(data: List<ArchEvent>) {
        events = data
        notifyDataSetChanged()
    }

    inner class ViewHolder(private val binding: ViewDataBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(event: ArchEvent) {
            binding.setVariable(BR.viewModel, ArchEventItemViewModel(event))
        }
    }
}