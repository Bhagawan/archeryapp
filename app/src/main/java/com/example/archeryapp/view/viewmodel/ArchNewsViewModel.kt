package com.example.archeryapp.view.viewmodel

import androidx.databinding.BaseObservable
import androidx.databinding.Bindable
import com.example.archeryapp.BR
import com.example.archeryapp.data.ArchNews
import com.example.archeryapp.util.ArchServerClient
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ArchNewsViewModel: BaseObservable() {

    @Bindable
    var news: List<ArchNews> = emptyList()

    init {
        ArchServerClient.create().getArchNews().enqueue(object : Callback<List<ArchNews>> {
            override fun onResponse(
                call: Call<List<ArchNews>>,
                response: Response<List<ArchNews>>
            ) {
                response.body()?.let {
                    news = it
                    notifyPropertyChanged(BR.news)
                }
            }
            override fun onFailure(call: Call<List<ArchNews>>, t: Throwable) {}
        })
    }

}