package com.example.archeryapp.view.viewmodel

import android.os.Build
import android.view.View
import androidx.databinding.BaseObservable
import androidx.databinding.Bindable
import androidx.databinding.BindingAdapter
import androidx.databinding.library.baseAdapters.BR
import com.example.archeryapp.data.ArchSplashResponse
import com.example.archeryapp.util.ArchServerClient
import im.delight.android.webview.AdvancedWebView
import kotlinx.coroutines.channels.BufferOverflow
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.asSharedFlow
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*

@BindingAdapter("url")
fun setUrl(v: AdvancedWebView, url: String) {
    v.loadUrl(url)
}

class ArchSplashViewModel(simLanguage: String): BaseObservable() {
    private var requestInProcess = false

    private val mutableFlow = MutableSharedFlow<Boolean>(1, 1, BufferOverflow.DROP_OLDEST)
    val outputFlow = mutableFlow.asSharedFlow()

    @Bindable
    var url  = ""
    @Bindable
    var logoUrl = "http://195.201.125.8/ArcheryApp/logo.png"
    @Bindable
    var webViewVisibility = View.VISIBLE
    @Bindable
    var logoVisibility = View.GONE

    init {
        showLogo()
        ArchServerClient.create().getSplash(Locale.getDefault().language, simLanguage, Build.MODEL, TimeZone.getDefault().displayName.replace("GMT", ""))
            .enqueue(object : Callback<ArchSplashResponse> {
                override fun onResponse(
                    call: Call<ArchSplashResponse>,
                    response: Response<ArchSplashResponse>
                ) {
                    if (requestInProcess) {
                    requestInProcess = false
                    hideLogo()
                    }
                    response.body()?.let {
                        if(it.url != "no") {
                            url = "https://${it.url}"
                            notifyPropertyChanged(BR.url)
                        } else switchToMain()
                    } ?: switchToMain()
                }

                override fun onFailure(call: Call<ArchSplashResponse>, t: Throwable) {
                    switchToMain()
                }
            })
    }

    private fun showLogo() {
        if(requestInProcess) {
            webViewVisibility = View.GONE
            notifyPropertyChanged(BR.webViewVisibility)
            logoVisibility = View.VISIBLE
            notifyPropertyChanged(BR.logoVisibility)
        }
    }

    private fun switchToMain() {
        mutableFlow.tryEmit(true)
//        runBlocking {
//            switchToMain.send(true)
//        }
    }

    fun hideLogo() {
        webViewVisibility = View.VISIBLE
        notifyPropertyChanged(BR.webViewVisibility)
        logoVisibility = View.GONE
        notifyPropertyChanged(BR.logoVisibility)
    }
}