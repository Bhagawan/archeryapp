package com.example.archeryapp.view.viewmodel

import com.example.archeryapp.data.ArchEvent

class ArchEventItemViewModel(val event: ArchEvent)