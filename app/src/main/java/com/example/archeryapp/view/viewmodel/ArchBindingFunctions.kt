package com.example.archeryapp.view.viewmodel

import android.widget.ImageView
import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.archeryapp.view.adapters.ArchBindableAdapter
import com.squareup.picasso.Picasso

@BindingAdapter("imageData")
fun setImageFromUrl(v: ImageView, url: String?) {
    Picasso.get().load(url).into(v)
}

@Suppress("UNCHECKED_CAST")
@BindingAdapter("recyclerData")
fun <T> setRecyclerData(v: RecyclerView, data: T) {
    if(v.adapter != null && data != null){
        if(v.adapter is ArchBindableAdapter<*>) {
            (v.adapter as ArchBindableAdapter<T>).setData(data)
        }
    }
}

class ArchBindingFunctions {}