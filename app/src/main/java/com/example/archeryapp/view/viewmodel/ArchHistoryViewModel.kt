package com.example.archeryapp.view.viewmodel

import androidx.databinding.BaseObservable
import androidx.databinding.Bindable
import com.example.archeryapp.BR
import com.example.archeryapp.data.ArchHistory
import com.example.archeryapp.util.ArchServerClient
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ArchHistoryViewModel: BaseObservable() {

    @Bindable
    var history: ArchHistory = ArchHistory(null, "")

    init {
        ArchServerClient.create().getArchHistory().enqueue(object : Callback<ArchHistory> {
            override fun onResponse(
                call: Call<ArchHistory>,
                response: Response<ArchHistory>
            ) {
                response.body()?.let {
                    history = it
                    notifyPropertyChanged(BR.history)
                }
            }
            override fun onFailure(call: Call<ArchHistory>, t: Throwable) {}
        })
    }
}