package com.example.archeryapp.view.ui

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.archeryapp.databinding.FragmentArcheryEventsBinding
import com.example.archeryapp.view.adapters.ArchEventsAdapter
import com.example.archeryapp.view.viewmodel.ArchEventsViewModel

class ArcheryEventsFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val binding = FragmentArcheryEventsBinding.inflate(layoutInflater)
        binding.viewModel = ArchEventsViewModel()
        binding.recyclerArchEvents.layoutManager = LinearLayoutManager(context)
        binding.recyclerArchEvents.adapter = ArchEventsAdapter()
        return binding.root
    }

}